# Network protocol specification
Ws2801 utilizes a custom binary network protocol. It consists of three message types:

* Command (c2s)
* Data (c2s)
* Response (s2c)

A command message is a general command to be sent to the server instance which can execute simple methods that are part of the `Ws2801Base` interface. Currently, the default implementation supports only two commands.

A data message contains a 64-bit integer representing the pixel number and three 8-bit grayscale values representing the red, green and blue value respectively.

WIP: A response message is a message from the server to the client that has to be requested manually. It contains just an 8-bit boolean value to indicate wheter the received message has been executed correctly or not.

## General message structure
All data is in little-endian format (most significant byte first).

* int64 – Message type (can be combined using bitwise or)
    * INVALID = 0x01: An invalid message has been received before
    * COMMAND = 0x02: Message type command
    * DATA = 0x04: Message type data
    * SYNC = 0x08: Request a response
    * RESPONSE = 0x10: Message type response
* Further data specific to each mesage type (see the respective subsections)

### Message type command
* uint8 – Command type (only one per message is recognized)
    * SHOW = 0x01 (send pixels to SPI, making any changes visible that happened before)
    * CLEAR = 0x02 (set all pixels to off/black)

### Message type data
* uint64 – Pixel number (index starting from 0)
* uint8 – Grayscale red amount
* uint8 – Grayscale green amount
* uint8 – Grayscale blue amount

### Message type response
* uint8 – `1 == true` for success, `0 == false` for no success (all values greater than 0 are still recognized as `true`)
