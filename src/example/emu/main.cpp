#include <QApplication>

#include <ws2801/ColorInterpolator.hpp>
#include <ws2801/Ws2801Emu.hpp>

int main(int argc, char** argv)
{
    QApplication app{argc, argv};

    Ws2801Emu emu;
    emu.showUi();

    ColorInterpolator ci;
    for (size_t i = 0; i < 32; i++) {
        emu.write(i, ci.rainbow(32, i));
    }
    emu.show();

    return app.exec();
}
