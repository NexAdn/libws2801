#include <iostream>
#include <thread>

#include <QApplication>

#include <ws2801/Ws2801Emu.hpp>
#include <ws2801/Ws2801Exception.hpp>
#include <ws2801/Ws2801RemoteServer.hpp>

int main(int argc, char** argv)
{
    QApplication app{argc, argv};
    try {
        Ws2801Emu wse;
        Ws2801RemoteServer ws{wse};
        wse.showUi();
        ws.start();

        return app.exec();
    }
    catch (Ws2801Exception e) {
        std::clog << "Caught Ws2801Exception " << e.errNo() << std::endl << e.what() << std::endl;
    }
    catch (...) {
        std::clog << "Unhandled exception. Aborting\n";
        ::exit(1);
    }
    return 1;
}
