#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>

#include "ws2801/Ws2801.hpp"

class TimeConverter
{
public:
    TimeConverter(std::chrono::system_clock::time_point t)
        : tp(t), tt(std::chrono::system_clock::to_time_t(t))
    {}

    std::chrono::system_clock::time_point& get()
    {
        return this->tp;
    }

    void update(std::chrono::system_clock::time_point t)
    {
        this->tp = t;
        this->tt = std::chrono::system_clock::to_time_t(t);
    }

    short hours() const
    {
        std::stringstream ss;
        ss << std::put_time(std::localtime(&this->tt), "%H");
        short res;
        ss >> res;
        return res;
    }
    short minutes() const
    {
        std::stringstream ss;
        ss << std::put_time(std::localtime(&this->tt), "%M");
        short res;
        ss >> res;
        return res;
    }
    short seconds() const
    {
        std::stringstream ss;
        ss << std::put_time(std::localtime(&this->tt), "%S");
        short res;
        ss >> res;
        return res;
    }

private:
    std::chrono::system_clock::time_point tp;
    time_t tt;
};

int main(int argc, char** argv)
{
    Ws2801 ws{};
    TimeConverter tc{std::chrono::system_clock::now()};

    while (true) {
        ws.clear();
        tc.update(std::chrono::system_clock::now());
        for (short i = 0; i < tc.hours(); i++) {
            ws.write(i, 0xff0000);
        }
        for (short i = 0; i < tc.minutes() / 2; i++) {
            ws.write(i, ws.read(i) | 0x00ff00);
        }
        for (short i = 0; i < tc.seconds() / 2; i++) {
            ws.write(i, ws.read(i) | 0x0000ff);
        }

        ws.write(29, 0x404040);

        if (tc.minutes() % 2 == 1) {
            ws.write(30, 0x00ff00);
        }
        if (tc.seconds() % 2 == 1) {
            ws.write(31, 0x0000ff);
        }
        ws.show();
        std::cerr << "\rTime: " << tc.hours() << ":" << tc.minutes() << " Uhr " << tc.seconds();
        std::this_thread::sleep_until(tc.get() + std::chrono::milliseconds{500});
    }

    return 0;
}
