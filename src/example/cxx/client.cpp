#include <iostream>

#include "ws2801/Ws2801Exception.hpp"
#include "ws2801/Ws2801RemoteClient.hpp"

int main()
{
    try {
        Ws2801RemoteClient ws{"localhost"};
        ws.write(0, 0xff0000);
        ws.write(1, 0x00ff00);
        ws.write(2, 0x0000ff);
        ws.write(3, 0xffffff);
        ws.show();
    }
    catch (Ws2801Exception e) {
        std::cout << "Caught " << e.errNo() << ": " << e.what() << std::endl;
        return 1;
    }
}
