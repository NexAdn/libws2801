#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <boost/system/system_error.hpp>

#include "ws2801/Ws2801.hpp"
#include "ws2801/Ws2801Exception.hpp"
#include "ws2801/Ws2801RemoteClient.hpp"

void printUsage(std::ostream& out)
{
    out << "Usage: cli [Options] [PixelAmount]\n"
        << "  Options:\n"
        << "    -l, --local            Use a locally connected ws2801\n"
        << "    -r, --remote [Address] Connect to a remote server at the given address\n"
        << "    -p, --port   [Port]    Connect to port number\n";
}

void printCmdUsage(std::ostream& out)
{
    out << "Available commands:\n"
        << "w, write [pixel] [r] [g] [b]    Write RGB to a pixel\n"
        << "s, show                         Make all changes visible (send to SPI)\n"
        << "c, clear                        Clear all pixels (all to off/black)\n"
        << "q, quit                         quit the program\n";
}

inline bool beginsWith(std::string lhs, std::string rhs)
{
    return (lhs.substr(0, rhs.size()) == rhs);
}

std::vector<std::string> getArgs(std::string cmdline)
{
    std::vector<std::string> res;
    std::string buf;
    for (std::size_t i = 0; i < cmdline.length(); i++) {
        if (cmdline.substr(i, 1) == " ") {
            res.push_back(buf);
            buf = "";
        } else {
            buf += cmdline.substr(i, 1);
        }
        if (i == cmdline.length() - 1 && buf != "") {
            res.push_back(buf);
        }
    }
    return res;
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        printUsage(std::clog);
        std::clog << "Too few arguments\n";
        return 1;
    }

    bool useLocal{false};
    bool useRemote{false};
    bool customPort{false};
    std::string remoteAddress;
    unsigned short customPortNo{0};
    for (std::size_t i = 1; i < argc; i++) {
        std::string v{argv[i]};
        if (v == "-l" || v == "--local") {
            if (useRemote) {
                printUsage(std::clog);
                std::clog << "Both local and remote defined\n";
                return -1;
            } else {
                useLocal = true;
            }
        }
        if (v == "-r" || v == "--remote") {
            if (useLocal) {
                printUsage(std::clog);
                std::clog << "Both local and remote defined\n";
                return -1;
            } else {
                useRemote = true;
                remoteAddress = argv[++i];
                std::cout << "Using remote address " << remoteAddress << std::endl;
                continue;
            }
        }
        if (v == "-p" || v == "--port") {
            customPort = true;
            std::istringstream iss{argv[++i]};
            iss >> customPortNo;
            continue;
        }
    }
    std::istringstream iss{argv[argc - 1]};
    std::size_t pixelAmount;
    iss >> pixelAmount;

    if (!useRemote && !useLocal) {
        printUsage(std::clog);
        std::clog << "Neither local nor remote defined\n";
        return 1;
    }

    std::unique_ptr<Ws2801Base> ws2801;
    try {
        if (useRemote) {
            ws2801 = std::make_unique<Ws2801RemoteClient>(
              remoteAddress, customPort ? customPortNo : 19638, pixelAmount);
        } else if (useLocal) {
            ws2801 = std::make_unique<Ws2801>(pixelAmount);
        }
        if (!ws2801) {
            return 1;
        }

        while (true) {
            std::cout << "> ";
            std::string input;
            std::cin.clear();
            std::getline(std::cin, input);
            if (input.length() < 1 || input == "")
                continue;

            auto args = getArgs(input);
            if (args[0] == "w" || args[0] == "write") {
                if (args.size() < 5) {
                    std::cout << "Invalid syntax.\n";
                    printCmdUsage(std::cout);
                    continue;
                } else {
                    std::size_t pixelNo;
                    short r, g, b;

                    std::cout << args.at(1) << std::endl;
                    pixelNo = boost::lexical_cast<short>(args.at(1));

                    std::cout << args.at(2) << std::endl;
                    r = boost::lexical_cast<short>(args.at(2));

                    std::cout << args.at(3) << std::endl;
                    g = boost::lexical_cast<short>(args.at(3));

                    std::cout << args.at(4) << std::endl;
                    b = boost::lexical_cast<short>(args.at(4));

                    std::cout << "Writing " << pixelNo << " (" << r << " " << g << " " << b
                              << ")\n";

                    if (!(r >= 0 && r < 256 && g >= 0 && g < 256 && b >= 0 && b < 256)) {
                        printCmdUsage(std::cout);
                        continue;
                    } else {
                        ws2801->write(pixelNo, r, g, b);
                    }
                }
            } else if (args[0] == "s" || args[0] == "show") {
                ws2801->show();
            } else if (args[0] == "c" || args[0] == "clear") {
                ws2801->clear();
            } else if (args[0] == "q" || args[0] == "quit") {
                std::cout << "Quit.\n";
                break;
            } else {
                printCmdUsage(std::cout);
                continue;
            }
        }
    }
    catch (Ws2801Exception e) {
        std::cerr << e.errNo() << " - " << e.what() << std::endl;
    }
    catch (boost::system::system_error e) {
        std::cerr << e.code() << " - " << e.what() << std::endl;
    }
    catch (std::exception e) {
        std::cerr << e.what() << std::endl;
    }
}
