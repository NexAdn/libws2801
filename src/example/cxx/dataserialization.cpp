#include <iomanip>
#include <iostream>

#include "ws2801/Ws2801Exception.hpp"
#include "ws2801/Ws2801RemoteData.hpp"

int main()
{
    // try {
    //     Ws2801RemoteData data{Ws2801RemoteDataType::INVALID, 9};
    //     data.addBool(true);
    //     std::int8_t res[data.getCurrentSize()];
    //     data.serialize(&res, sizeof(res));
    //     for (std::size_t i = 0; i < data.getCurrentSize(); i++) {
    //         if (i % 4 == 0) {
    //             std::cout << std::endl;
    //         }
    //         std::cout << std::setfill('0') << std::setw(2) << std::hex <<
    //         static_cast<int>(res[i])
    //                   << " ";
    //     }
    //     std::cout << "\n\n== == == ==\n";
    //     std::size_t size{data.getCurrentSize()};
    //     for (std::size_t i = 0; i < size; i++) {
    //         if (i % 4 == 0) {
    //             std::cout << std::endl;
    //         }
    //         std::cout << std::setfill('0') << std::setw(2) << std::hex
    //                   << static_cast<unsigned int>(data.get<std::int8_t>()) << " ";
    //     }
    //     std::cout << std::endl;
    // }
    // catch (Ws2801Exception e) {
    //     std::cout << "Caught Ws2801Exception " << e.errNo() << std::endl << e.what() <<
    //     std::endl;
    // }
    // catch (...) {
    //     std::cout << "Unhandled exception. Aborting\n";
    //     ::exit(1);
    // }
}
