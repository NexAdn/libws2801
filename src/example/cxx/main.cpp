#include <iostream>

#include "ws2801/Ws2801.hpp"

namespace
{
constexpr size_t NUM_PIXELS{32};
}

int main(int argc, char** argv)
{
    Ws2801 ws{NUM_PIXELS};
    if (!ws) {
        std::cerr << "Failed to initialize ws2801. Quitting.\n";
        return 0;
    }

    std::cout << "Writing to buffer\n";
    for (size_t i = 0; i < NUM_PIXELS; i++) {
        switch (i % 3) {
        case 0:
            ws.write(i, 255, 0, 0);
            break;
        case 1:
            ws.write(i, 0, 255, 0);
            break;
        case 2:
            ws.write(i, 0, 0, 255);
            break;
        }
    }
    std::cout << "Writing to SPI\n";
    ws.show();

    return 0;
}
