#include <cmath>
#include <thread>

#include "ws2801/ColorInterpolator.hpp"
#include "ws2801/Ws2801.hpp"

class Metaball
{
public:
    Metaball(int mass, double speed, uint8_t pos) : mass(mass), speed(speed), pos(pos)
    {}

    uint8_t getPos() const
    {
        return static_cast<uint8_t>(this->pos);
    }

    int getMass() const
    {
        return this->mass;
    }

    double getSpeed() const
    {
        return this->speed;
    }

    void setSpeed(double speed)
    {
        this->speed = speed;
    }

    Metaball& move()
    {
        this->pos += this->speed;
        if (this->pos < 0) {
            this->pos = 0;
            this->speed = -this->speed;
        } else if (this->pos > 255) {
            this->pos = 255;
            this->speed = -this->speed;
        }
        return *this;
    }

    void collide(Metaball& rhs)
    {
        int buf = this->speed;
        this->speed =
          ((this->mass - rhs.getMass()) * this->speed + 2 * rhs.getMass() * rhs.getSpeed())
          / (this->mass + rhs.getMass());
        rhs.setSpeed(((rhs.getMass() - this->mass) * rhs.getSpeed() + 2 * this->mass * buf)
                     / (this->mass + rhs.getMass()));
    }

private:
    const int mass;
    double speed;
    int16_t pos;
};

inline uint32_t colorSegment(uint8_t ppos, const Metaball& mb1, const Metaball& mb2)
{
    double p = 3.5 + 8 * ppos;
    short radius = 2048;
    return ColorInterpolator::getColor(static_cast<int>(radius / std::fabs(mb1.getPos() - p) + 1)
                                       + radius / (std::fabs(mb2.getPos() - p) + 1));
}

int main()
{
    Ws2801 ws{};
    Metaball mb1{1, .2, 4};
    Metaball mb2{3, -.2, 200};
    while (true) {
        auto tp = std::chrono::system_clock::now();
        if (mb1.move().getPos() >= mb2.move().getPos()) {
            mb1.collide(mb2);
        }
        for (uint8_t i = 0; i < 32; i++) {
            ws.write(i, colorSegment(i, mb1, mb2));
        }
        ws.show();
        std::this_thread::sleep_until(tp + std::chrono::milliseconds{10});
    }
}
