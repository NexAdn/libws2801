#include <iostream>
#include <memory>

#include <ws2801/Ws2801.hpp>
#include <ws2801/Ws2801Base.hpp>
#include <ws2801/Ws2801Exception.hpp>
#include <ws2801/Ws2801RemoteServer.hpp>

int main()
{
    try {
        Ws2801 wse{32};
        Ws2801RemoteServer ws(wse);
        ws.start();
        while (ws.isRunning())
            ::sched_yield();
    }
    catch (Ws2801Exception e) {
        std::cout << "Caught Ws2801Exception " << e.errNo() << std::endl << e.what() << std::endl;
    }
    catch (...) {
        std::cout << "Unhandled exception. Aborting\n";
        ::exit(1);
    }
}
