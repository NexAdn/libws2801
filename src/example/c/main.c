#include "ws2801/ws2801.h"

#include <stdio.h>

#define NUM_PIXELS 32

int main(int argc, char** argv)
{
    ws2801_t* ws = ws2801_create(NUM_PIXELS);
    if (ws2801_init(ws) < 0) {
        fprintf(stderr, "Failed to initialize ws2801. Quitting.\n");
        return 0;
    }

    printf("Writing to buffer\n");
    for (size_t i = 0; i < NUM_PIXELS; i++) {
        switch (i % 3) {
        case 0:
            ws2801_write(ws, i, ws2801_ctoi(255, 0, 0));
            break;
        case 1:
            ws2801_write(ws, i, ws2801_ctoi(0, 255, 0));
            break;
        case 2:
            ws2801_write(ws, i, ws2801_ctoi(0, 0, 255));
            break;
        }
        printf("Pixel %d: %X%X%X\n", (int) i, ws->pixels[i * 3], ws->pixels[i * 3 + 1],
               ws->pixels[i * 3 + 2]);
    }
    printf("Writing to SPI\n");
    ws2801_show(ws);

    ws2801_deinit(ws);
    ws2801_destroy(ws);
    return 0;
}
