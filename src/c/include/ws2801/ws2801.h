/**
 * @file ws2801.h
 * @brief C interface to libws2801.
 */

#ifndef WS2801_H__
#define WS2801_H__
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

/**
 * Main SPI device data structure
 *
 * This structure holds the file descriptor used to access the SPI device, the pixel number and an
 * array of bytes to store all grayscale pixel values.
 *
 * Memory allocation and deallocation take place in {@link ws2801_create} and {@link ws2801_destroy}
 * respectively.
 */
typedef struct ws2801
{
    /** The file descriptor */
    int fd;
    /** The amount of ws2801 pixels connected (1m strip should equal 32 pixels). Must never be
     * changed! */
    size_t num_pixels;
    /**
     * Array containing all grayscale pixel colors values
     *
     * The array has three times the value of {@link #num_pixels}.
     * It is a continuous array where at each position n % 3 = 0 there is a 24 bit word consisting
     * of three 8 bit grayscale color values, representing the R, G and B values of pixel n / 3
     * respectively.
     */
    uint8_t* pixels;
} ws2801_t;

/**
 * Allocates an instance of {@link ws2801_t} without initializing it and sets all pixels to off.
 *
 * @param  num_pixels The amount of pixels connected to the ws2801 (1m strip should equal 32
 * pixels).
 */
ws2801_t* ws2801_create(size_t num_pixels);
/**
 * Deallocates an instance of {@link ws2801_t}. Deinitialization is required before performing
 * this operation.
 */
void ws2801_destroy(ws2801_t* ws);

/**
 * Initializes a {@link ws2801_t}
 *
 * @return    0, if everything went well. A number less than 0 indicates an initialization error.
 */
int ws2801_init(ws2801_t* ws);
/**
 * Deinitialize a {@link ws2801_t}
 */
void ws2801_deinit(ws2801_t* ws);
/**
 * Write a color to a pixel into the pixel store.
 *
 * Info: This function does not send the new pixel to SPI. Instead the color information is written
 * to the internal pixel store. The pixels can be sent altogether to SPI with {@link ws2801_show}.
 *
 * @param ws     The ws2801 to write to
 * @param pix_no The pixel to write to (starting with 0 as the first pixel.
 * @param color  A 24 bit word containing the color information as returned by {@link ws2801_ctoi}.
 */
void ws2801_write(ws2801_t* ws, size_t pix_no, uint32_t color);
/**
 * Writes the internal pixel store to SPI.
 *
 * This method is the only method performing an update on the ws2801 LED pixels. Every set of
 * changes requires this method to be called afterwards for pixel/color changes to take effect.
 */
void ws2801_show(ws2801_t* ws);

/**
 * Convert 8 bit grayscale values to 24 bit RGB color values
 * @return   A 24 bit RGB color value. The leftmost byte is left out, RGB color information is
 * written left to right after the first byte.
 */
uint32_t ws2801_ctoi(uint8_t r, uint8_t g, uint8_t b);

#ifdef __cplusplus
}
#endif

#endif // WS2801_H__
