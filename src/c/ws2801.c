#include "ws2801/ws2801.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>

#include "spi.h"

static const struct timespec SPI_SLEEP = {.tv_sec = 0, .tv_nsec = 500 * 1000};

#define WS2801_MOSI 10
#define WS2801_SCLK 11

static uint8_t* get_pixel(ws2801_t* this, size_t num)
{
    if (num >= this->num_pixels)
        return NULL;
    return &this->pixels[num * 3];
}

static uint32_t get_pixel_color(ws2801_t* this, size_t num)
{
    if (num >= this->num_pixels)
        return 0;
    uint8_t* r = get_pixel(this, num);
    return ws2801_ctoi(r[0], r[1], r[2]);
}

ws2801_t* ws2801_create(size_t num_pixels)
{
    ws2801_t* this = malloc(sizeof(ws2801_t));
    memset(this, 0, sizeof(ws2801_t));
    this->num_pixels = num_pixels;
    this->pixels = malloc(sizeof(uint8_t) * 3 * this->num_pixels);
    memset(this->pixels, 0, sizeof(ws2801_t));
    return this;
}

void ws2801_destroy(ws2801_t* this)
{
    free(this->pixels);
    free(this);
}

int ws2801_init(ws2801_t* this)
{
    if ((this->fd = spi_open(0, 0)) < 0) {
        fprintf(stderr, "ws2801_init(): spi_open() failed to get file descriptor!\n");
        return -1;
    }
    return 0;
}

void ws2801_deinit(ws2801_t* this)
{
    spi_close(this->fd);
}

void ws2801_write(ws2801_t* this, size_t pin, uint32_t color)
{
    if (pin >= this->num_pixels)
        return;
    uint8_t* r = get_pixel(this, pin);
    r[0] = (color & 0x00ff0000) >> 16;
    r[1] = (color & 0x0000ff00) >> 8;
    r[2] = (color & 0x000000ff);
}

void ws2801_show(ws2801_t* this)
{
    for (size_t i = 0; i < this->num_pixels; i++)
        spi_write(this->fd, &this->pixels[i * 3]);
    nanosleep(&SPI_SLEEP, NULL);
}

uint32_t ws2801_ctoi(uint8_t r, uint8_t g, uint8_t b)
{
    return (r << 16) | (g << 8) | b;
}
