/**
 * @file spi.h
 * @brief Internal SPI interface
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

int spi_open(uint8_t bus, uint8_t chip_select);
void spi_close(int fd);

// uint8_t* needs to point to a 24-bit RGB numeric color value!
void spi_write(int fd, uint8_t* data);

#ifdef __cplusplus
}
#endif
