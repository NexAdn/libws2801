/*
 * Code adapted and modified from source: https://github.com/piface/libmcp23s17.git
 */
#include "spi.h"

#include <errno.h>
#include <fcntl.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

static const char* SPI_DEVS[2][2] = {{"/dev/spidev0.0", "/dev/spidev0.1"},
                                     {"/dev/spidev1.0", "/dev/spidev1.1"}};

static const uint8_t spi_mode = 0;
static const uint8_t spi_bpw = 8;          // bits per word
static const uint32_t spi_speed = 2000000; // 10MHz
static const uint16_t spi_delay = 0;

int spi_open(uint8_t bus, uint8_t chip_select)
{
    int fd;
    // open
    if ((fd = open(SPI_DEVS[bus][chip_select], O_RDWR | O_NONBLOCK)) < 0) {
        fprintf(stderr, "spi_open(): ERROR Could not open SPI device (%s).\n",
                SPI_DEVS[bus][chip_select]);
        return -1;
    }

    // initialise
    if (ioctl(fd, SPI_IOC_WR_MODE, &spi_mode) < 0) {
        fprintf(stderr, "spi_open(): ERROR Could not set SPI mode.\n");
        close(fd);
        return -1;
    }
    if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &spi_bpw) < 0) {
        fprintf(stderr, "spi_open(): ERROR Could not set SPI bits per word.\n");
        close(fd);
        return -1;
    }
    if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed) < 0) {
        fprintf(stderr, "spi_open(): ERROR Could not set SPI speed.\n");
        close(fd);
        return -1;
    }

    return fd;
}

void spi_close(int fd)
{
    close(fd);
}

void spi_write(int fd, uint8_t* data)
{
    // <magic> spi.tx_buf somehow needs to be an array.
    uint8_t tx_buf[] = {data[0], data[1], data[2]};
    uint8_t rx_buf[sizeof(tx_buf)];
    struct spi_ioc_transfer spi;
    memset(&spi, 0, sizeof(spi));
    spi.tx_buf = (unsigned long) tx_buf;
    spi.rx_buf = (unsigned long) rx_buf;
    spi.len = sizeof(tx_buf);
    // </magic>
    spi.delay_usecs = spi_delay;
    spi.speed_hz = spi_speed;
    spi.bits_per_word = spi_bpw;

    // do the SPI transaction
    if ((ioctl(fd, SPI_IOC_MESSAGE(1), &spi) < 0)) {
        fprintf(stderr, "spi_write(): There was a error during the SPI transaction.\n");
    }
}
