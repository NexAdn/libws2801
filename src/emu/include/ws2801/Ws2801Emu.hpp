#ifndef WS2801EMU_HPP__
#define WS2801EMU_HPP__
#pragma once

#include <array>
#include <memory>

#include <QMainWindow>

#include "ws2801/Ws2801Base.hpp"

namespace Ui
{
class Ws2801EmuMainWindow;
} // namespace Ui

class QLCDNumber;

// QObject must be inherited as first parent!
class Ws2801Emu : public QMainWindow, public Ws2801Base
{
    Q_OBJECT

public:
    explicit Ws2801Emu(QWidget* parent = nullptr);
    ~Ws2801Emu();

    operator bool() const override;

    void write(std::size_t pixelNo, std::uint32_t color, bool instant = false) override;
    void write(std::size_t pixelNo, std::uint8_t r, std::uint8_t g, std::uint8_t b,
               bool instant = false) override;

    std::uint32_t read(std::size_t pixelNo) const override;

    void show() const override;
    void showUi();

    void clear(bool instant = false) override;

signals:
    void uiNeedsRefresh() const;

public slots:
    void refreshUi();

private:
    std::array<std::uint32_t, 32> pixels;

    std::unique_ptr<Ui::Ws2801EmuMainWindow> ui;
    std::array<QLCDNumber*, 32> uiOutputs;
};

#endif // WS2801EMU_HPP__
