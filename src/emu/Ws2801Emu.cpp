#include "ws2801/Ws2801Emu.hpp"

#include <iostream>
#include <sstream>

#include <QLCDNumber>

#include "ws2801/Ws2801.hpp"

#include "ui_window.h"

Ws2801Emu::Ws2801Emu(QWidget* parent)
    : QMainWindow(parent), ui(std::make_unique<Ui::Ws2801EmuMainWindow>())
{
    this->ui->setupUi(this);

    this->pixels.fill(0);

    this->uiOutputs = {
      ui->pixNo_0,  ui->pixNo_1,  ui->pixNo_2,  ui->pixNo_3,  ui->pixNo_4,  ui->pixNo_5,
      ui->pixNo_6,  ui->pixNo_7,  ui->pixNo_8,  ui->pixNo_9,  ui->pixNo_10, ui->pixNo_11,
      ui->pixNo_12, ui->pixNo_13, ui->pixNo_14, ui->pixNo_15, ui->pixNo_16, ui->pixNo_17,
      ui->pixNo_18, ui->pixNo_19, ui->pixNo_20, ui->pixNo_21, ui->pixNo_22, ui->pixNo_23,
      ui->pixNo_24, ui->pixNo_25, ui->pixNo_26, ui->pixNo_27, ui->pixNo_28, ui->pixNo_29,
      ui->pixNo_30, ui->pixNo_31,
    };

    connect(this, SIGNAL(uiNeedsRefresh()), this, SLOT(refreshUi()),
            Qt::ConnectionType::QueuedConnection);

    emit this->uiNeedsRefresh();
}

Ws2801Emu::~Ws2801Emu()
{}

Ws2801Emu::operator bool() const
{
    return true;
}

void Ws2801Emu::write(std::size_t pixelNo, std::uint32_t color, bool instant)
{
    if (pixelNo >= this->pixels.size())
        return;

    this->pixels.at(pixelNo) = color;

    if (instant)
        this->show();
}

void Ws2801Emu::write(std::size_t pixelNo, std::uint8_t r, std::uint8_t g, std::uint8_t b,
                      bool instant)
{
    this->write(pixelNo, Ws2801::ctoi(r, g, b), instant);
}

std::uint32_t Ws2801Emu::read(std::size_t pixelNo) const
{
    if (pixelNo >= this->pixels.size())
        return 0x000000;

    return this->pixels.at(pixelNo);
}

void Ws2801Emu::show() const
{
    emit this->uiNeedsRefresh();
}

void Ws2801Emu::showUi()
{
    QWidget::show();
}

void Ws2801Emu::clear(bool instant)
{
    this->pixels.fill(0);
    if (instant)
        this->show();
}

void Ws2801Emu::refreshUi()
{
    for (std::uint8_t i = 0; i < 32; i++) {
        std::stringstream sheet;
        auto color{Ws2801::itoc(this->pixels.at(i))};
        sheet << "background-color: rgb(" << static_cast<int>(std::get<0>(color)) << ","
              << static_cast<int>(std::get<1>(color)) << "," << static_cast<int>(std::get<2>(color))
              << ");";
        this->uiOutputs.at(i)->setStyleSheet(sheet.str().c_str());
    }
}
