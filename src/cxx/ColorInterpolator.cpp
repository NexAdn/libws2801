#include "ws2801/ColorInterpolator.hpp"

#include "ws2801/Ws2801.hpp"

ColorInterpolator::ColorInterpolator(const uint8_t pos) : pos(pos)
{}

ColorInterpolator::~ColorInterpolator()
{}

ColorInterpolator::operator uint32_t() const
{
    return ColorInterpolator::getColor(this->pos);
}

ColorInterpolator::operator std::tuple<uint8_t, uint8_t, uint8_t>() const
{
    return Ws2801::itoc(ColorInterpolator::getColor((this->pos)));
}

uint32_t ColorInterpolator::rainbow(const size_t numPixels, const size_t pixelNo) const
{
    return ColorInterpolator::getColor((pixelNo * 256 / numPixels + this->pos) % 256);
}

ColorInterpolator& ColorInterpolator::inc(const uint8_t pos)
{
    this->pos += pos;
    return *this;
}

ColorInterpolator& ColorInterpolator::dec(const uint8_t pos)
{
    this->pos -= pos;
    return *this;
}

uint32_t ColorInterpolator::getColor(const uint8_t pos)
{
    // “wheel“ function from Adafruit's ws2801 examples (github.com/adafruit/adafruit_python_ws2801)
    if (pos < 85) {
        return Ws2801::ctoi(pos * 3, 255 - pos * 3, 0);
    } else if (pos < 170) {
        return Ws2801::ctoi(255 - pos * 3, 0, pos * 3);
    } else {
        return Ws2801::ctoi(0, pos * 3, 255 - pos * 3);
    }
}
