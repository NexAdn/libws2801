#include "ws2801/Ws2801Exception.hpp"

Ws2801Exception::Ws2801Exception(int errNo, std::string what) : errNo_(errNo), what_(what)
{}

const char* Ws2801Exception::what()
{
    return this->what_.c_str();
}

int Ws2801Exception::errNo()
{
    return this->errNo_;
}
