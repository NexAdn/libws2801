#include "ws2801/Ws2801RemoteData.hpp"

#include "ws2801/Ws2801Exception.hpp"

Ws2801RemoteDataType operator|(const Ws2801RemoteDataType lhs, const Ws2801RemoteDataType rhs)
{
    return static_cast<Ws2801RemoteDataType>(static_cast<std::int64_t>(lhs)
                                             | static_cast<std::int64_t>(rhs));
}

bool operator==(const Ws2801RemoteDataType lhs, const Ws2801RemoteDataType rhs)
{
    return (static_cast<std::int64_t>(lhs) & static_cast<std::int64_t>(rhs)) != 0;
}

Ws2801RemoteData::Ws2801RemoteData(Ws2801RemoteDataType rdt, std::size_t maxSize) : maxSize(maxSize)
{
    this->values.empty();
    std::int64_t type = static_cast<std::int64_t>(rdt);
    this->addInt64(type);
}

Ws2801RemoteData::Ws2801RemoteData(const void* data, std::size_t dataSize) : maxSize(dataSize)
{
    auto* dat = reinterpret_cast<const std::int8_t*>(data);
    this->values.empty();
    for (std::size_t i = 0; i < dataSize; i++) {
        this->values.push_back(dat[i]);
    }
}

Ws2801RemoteData::Ws2801RemoteData(std::vector<std::uint8_t> data) : maxSize(data.size() - 2)
{
    this->values.empty();
    for (const auto& d : data) {
        this->values.push_back(d);
    }
}

std::vector<std::uint8_t> Ws2801RemoteData::serialize() const
{
    std::vector<std::uint8_t> data;
    data.empty();
    for (const auto& v : this->values) {
        data.push_back(static_cast<std::uint8_t>(v));
    }

    return data;
}

std::size_t Ws2801RemoteData::getCurrentSize() const
{
    return this->values.size();
}

std::size_t Ws2801RemoteData::getMaxSize() const
{
    return this->maxSize;
}

Ws2801RemoteData& Ws2801RemoteData::addBool(bool val) noexcept
{
    this->addUInt8(val ? 0x1 : 0x0);
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addInt8(std::int8_t val) noexcept
{
    if (this->values.size() < this->maxSize) // Ensure max size
        this->values.push_back(val);
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addUInt8(std::uint8_t val) noexcept
{
    this->addInt8(*(reinterpret_cast<std::int8_t*>(&val)));
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addInt16(std::int16_t val) noexcept
{
    this->addInt8((val & 0xff00) >> 8);
    this->addInt8(val & 0x00ff);
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addUInt16(std::uint16_t val) noexcept
{
    this->addInt16(*(reinterpret_cast<std::int16_t*>(&val)));
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addInt32(std::int32_t val) noexcept
{
    this->addInt16((val & 0xffff0000) >> 16);
    this->addInt16(val & 0x0000ffff);
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addUInt32(std::uint32_t val) noexcept
{
    this->addInt32(*(reinterpret_cast<std::int32_t*>(&val)));
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addInt64(std::int64_t val) noexcept
{
    this->addInt32((val & 0xffffffff00000000) >> 32);
    this->addInt32(val & 0x00000000ffffffff);
    return *this;
}

Ws2801RemoteData& Ws2801RemoteData::addUInt64(std::uint64_t val) noexcept
{
    this->addInt64(*(reinterpret_cast<std::int64_t*>(&val)));
    return *this;
}

template <>
std::int8_t Ws2801RemoteData::get<std::int8_t>()
{
    if (this->values.size() < sizeof(std::int8_t))
        throw Ws2801Exception(201, "Ws2801RemoteData::get(): Request larger than available buffer");
    std::int8_t data{this->values.front()};
    this->values.pop_front();
    return data;
}

template <>
std::uint8_t Ws2801RemoteData::get<std::uint8_t>()
{
    std::int8_t res{this->get<std::int8_t>()};
    return *(reinterpret_cast<std::uint8_t*>(&res));
}

template <>
bool Ws2801RemoteData::get<bool>()
{
    return this->get<std::uint8_t>() > 0 ? true : false;
}

template <>
std::int16_t Ws2801RemoteData::get<std::int16_t>()
{
    return (static_cast<std::int16_t>(this->get<std::int8_t>()) << 8) | this->get<std::int8_t>();
}

template <>
std::uint16_t Ws2801RemoteData::get<std::uint16_t>()
{
    std::int16_t res{this->get<std::int16_t>()};
    return *(reinterpret_cast<std::uint16_t*>(&res));
}

template <>
std::int32_t Ws2801RemoteData::get<std::int32_t>()
{
    return (static_cast<std::int32_t>(this->get<std::int16_t>()) << 16) | this->get<std::int16_t>();
}

template <>
std::uint32_t Ws2801RemoteData::get<std::uint32_t>()
{
    std::int32_t res{this->get<std::int32_t>()};
    return *(reinterpret_cast<std::uint32_t*>(&res));
}

template <>
std::int64_t Ws2801RemoteData::get<std::int64_t>()
{
    return (static_cast<std::int64_t>(this->get<std::int32_t>()) << 32) | this->get<std::int32_t>();
}

template <>
std::uint64_t Ws2801RemoteData::get<std::uint64_t>()
{
    std::int64_t res{this->get<std::int64_t>()};
    return *(reinterpret_cast<std::uint64_t*>(&res));
}
