#include "ws2801/Ws2801RemoteClient.hpp"

#include <cstring>
#include <sstream>

#include "ws2801/Ws2801.hpp"
#include "ws2801/Ws2801Exception.hpp"
#include "ws2801/Ws2801RemoteData.hpp"

Ws2801RemoteClient::Ws2801RemoteClient(std::string address, unsigned short portNo, size_t pixelNo)
    : numPixels(pixelNo)
{
    this->pixels.empty();
    this->pixels.resize(this->numPixels);
    for (size_t i = 0; i < this->numPixels; i++)
        this->pixels.at(i) = 0x000000u;

    try {
        boost::asio::ip::tcp::resolver resolver{this->ioContext};
        std::stringstream ss;
        ss << portNo;
        auto endpoints{resolver.resolve(address, ss.str())};

        this->socket = std::make_unique<boost::asio::ip::tcp::socket>(this->ioContext);

        boost::asio::connect(*this->socket, endpoints);
    }
    catch (boost::system::system_error e) {
        throw Ws2801Exception(001, std::string{"Ws2801RemoteClient(): Unhandled boost exception: "}
                                     + std::string{e.code().message()});
    }
    catch (std::exception e) {
        throw Ws2801Exception(000, std::string{"Ws2801RemoteClient(): Unhandled exception: "}
                                     + std::string{e.what()});
    }
}

Ws2801RemoteClient::~Ws2801RemoteClient()
{}

Ws2801RemoteClient::operator bool() const
{
    return true;
}

void Ws2801RemoteClient::write(size_t pixelNo, uint32_t color, bool instant)
{
    auto c = Ws2801::itoc(color);
    this->write(pixelNo, std::get<0>(c), std::get<1>(c), std::get<2>(c), instant);
}

void Ws2801RemoteClient::write(size_t pixelNo, uint8_t red, uint8_t green, uint8_t blue,
                               bool instant)
{
    this->pixels.at(pixelNo) = Ws2801::ctoi(red, green, blue);

    Ws2801RemoteData data{Ws2801RemoteDataType::DATA, Ws2801RemoteData::c2sDataSize};
    data.addUInt64(pixelNo).addUInt8(red).addUInt8(green).addUInt8(blue);

    boost::asio::write(*this->socket, boost::asio::buffer(data.serialize()));

    if (instant)
        this->show();
}

uint32_t Ws2801RemoteClient::read(size_t pixelNo) const
{
    try {
        return this->pixels.at(pixelNo);
    }
    catch (std::out_of_range) {
        return 0x000000u;
    }
}

void Ws2801RemoteClient::show() const
{
    Ws2801RemoteData data{Ws2801RemoteDataType::COMMAND, Ws2801RemoteData::c2sCommandSize};
    data.addUInt8(static_cast<std::uint8_t>(Ws2801RemoteDataCommandType::SHOW));

    boost::asio::write(*this->socket, boost::asio::buffer(data.serialize()));
}

void Ws2801RemoteClient::clear(bool instant)
{
    Ws2801RemoteData data{Ws2801RemoteDataType::COMMAND, Ws2801RemoteData::c2sCommandSize};
    data.addUInt8(static_cast<std::uint8_t>(Ws2801RemoteDataCommandType::CLEAR));

    boost::asio::write(*this->socket, boost::asio::buffer(data.serialize()));

    if (instant)
        this->show();
}
