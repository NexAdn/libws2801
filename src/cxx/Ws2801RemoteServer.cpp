#include "ws2801/Ws2801RemoteServer.hpp"

#include <array>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <tuple>
#include <vector>

#include <unistd.h>

#include "ws2801/Ws2801.hpp"
#include "ws2801/Ws2801Exception.hpp"
#include "ws2801/Ws2801RemoteData.hpp"

namespace
{
void sendPositiveResponse(boost::asio::ip::tcp::socket& socket)
{
    boost::asio::write(
      socket,
      boost::asio::buffer(
        Ws2801RemoteData{Ws2801RemoteDataType::RESPONSE, 9}.addBool(true).serialize()));
}
} // namespace

Ws2801RemoteServer::Ws2801RemoteServer(Ws2801Base& ws, unsigned short portNo) : ws(ws)
{
    using namespace boost::asio;
    try {
        this->acceptor = std::make_unique<ip::tcp::acceptor>(
          this->ioContext, ip::tcp::endpoint{ip::tcp::v4(), portNo});
    }
    catch (std::exception e) {
        throw Ws2801Exception(000, std::string{"Ws2801RemoteServer(): Unhandled exception:"}
                                     + std::string{e.what()});
    }
}

Ws2801RemoteServer::~Ws2801RemoteServer()
{
    this->stop();
    if (this->requestBufferWorkerThread.joinable())
        this->requestBufferWorkerThread.join();
}

Ws2801RemoteServer::operator bool() const
{
    return static_cast<bool>(this->ws);
}

void Ws2801RemoteServer::start()
{
    this->run = true;
    this->requestBufferWorkerThread = std::thread{[this]() { this->requestBufferWorker(); }};
    this->networkListenerThread = std::thread{[this]() { this->networkListener(); }};
}

bool Ws2801RemoteServer::isRunning() const
{
    return this->run;
}

void Ws2801RemoteServer::stop()
{
    this->run = false;
    if (this->networkListenerThread.joinable())
        this->networkListenerThread.join();
    for (auto& t : this->networkHandlerThreads) {
        if (t.joinable())
            t.join();
    }
}

void Ws2801RemoteServer::write(size_t pixelNo, uint32_t color, bool instant)
{
    this->ws.write(pixelNo, color, instant);
}

void Ws2801RemoteServer::write(size_t pixelNo, uint8_t r, uint8_t g, uint8_t b, bool instant)
{
    this->write(pixelNo, Ws2801::ctoi(r, g, b), instant);
}

uint32_t Ws2801RemoteServer::read(size_t pixelNo) const
{
    return this->ws.read(pixelNo);
}

void Ws2801RemoteServer::show() const
{
    this->ws.show();
}

void Ws2801RemoteServer::clear(bool instant)
{
    this->ws.clear(instant);
}

void Ws2801RemoteServer::networkListener()
{
    using namespace boost::asio;
    try {
        static std::int64_t lastSessionId{std::rand()};

        while (this->run) {
            ip::tcp::socket socket{this->ioContext};
            this->acceptor->accept(socket);
            std::cout << "Accepting new connection\n";

            this->networkHandlerThreads.push_back(std::thread{
              [this](ip::tcp::socket socket) {
                  try {
                      bool nhtRun{true};
                      while (nhtRun) {
                          std::array<std::uint8_t, Ws2801RemoteData::c2sDataSize> buf;
                          buf.fill(0);
                          boost::system::error_code error;
                          std::size_t read{socket.read_some(buffer(buf, buf.max_size()), error)};
                          if (error) {
                              if (error == error::eof) {
                                  std::cout << "Closing connection\n";
                                  nhtRun = false;
                              } else {
                                  throw boost::system::system_error{error};
                              }
                          }

                          Ws2801RemoteData data{std::vector<std::uint8_t>{buf.begin(), buf.end()}};
                          Ws2801RemoteDataType dataType{
                            static_cast<Ws2801RemoteDataType>(data.get<std::int64_t>())};

                          if (dataType == Ws2801RemoteDataType::COMMAND) {
                              switch (static_cast<Ws2801RemoteDataCommandType>(
                                data.get<std::uint8_t>())) {
                              case Ws2801RemoteDataCommandType::SHOW: {
#ifndef WS2801_NOSPI
                                  {
                                      std::lock_guard<std::mutex> lk{this->requestBufferMutex};
                                      this->requestBuffer.push([this]() { this->show(); });
                                  }
#endif
                                  if (dataType == Ws2801RemoteDataType::SYNC) {
                                      sendPositiveResponse(socket);
                                  }
#ifdef WS2801_DEBUG
                                  std::cout << "SHOW\n";
#endif
                                  break;
                              }
                              case Ws2801RemoteDataCommandType::CLEAR: {
#ifndef WS2801_NOSPI
                                  {
                                      std::lock_guard<std::mutex> lk{this->requestBufferMutex};
                                      this->requestBuffer.push([this]() { this->clear(false); });
                                  }
#endif
                                  if (dataType == Ws2801RemoteDataType::SYNC) {
                                      sendPositiveResponse(socket);
                                  }
#ifdef WS2801_DEBUG
                                  std::cout << "CLEAR\n";
#endif
                                  break;
                              }
                              }
                          } else if (dataType == Ws2801RemoteDataType::DATA) {
#ifndef WS2801_NOSPI
                              {
                                  auto pix{data.get<std::uint64_t>()};
                                  auto r{data.get<std::uint8_t>()};
                                  auto g{data.get<std::uint8_t>()};
                                  auto b{data.get<std::uint8_t>()};

                                  std::lock_guard<std::mutex> lk{this->requestBufferMutex};
                                  this->requestBuffer.push(
                                    [this, pix, r, g, b]() { this->write(pix, r, g, b); });
                              }
#endif
                              if (dataType == Ws2801RemoteDataType::SYNC) {
                                  sendPositiveResponse(socket);
                              }
#ifdef WS2801_DEBUG
                              std::cout
                                << "DATA: " << std::hex
                                << static_cast<unsigned int>(data.get<std::uint64_t>()) << " - "
                                << static_cast<unsigned int>(data.get<std::uint8_t>()) << " "
                                << static_cast<unsigned int>(data.get<std::uint8_t>()) << " "
                                << static_cast<unsigned int>(data.get<std::uint8_t>()) << std::endl;
#endif
                          }

                          if (!this->run) {
                              socket.close();
                              break;
                          }
                      }
                  }
                  catch (std::exception e) {
                      std::cerr << "Caught an exception in network handler thread:\n"
                                << "  what(): " << e.what() << std::endl;
                  }
              },
              std::move(socket)});
            ::sched_yield();
        }
    }
    catch (Ws2801Exception e) {
        throw e;
    }
    catch (std::exception e) {
        throw Ws2801Exception(000, std::string{"Unhandled exception in network listener thread: "}
                                     + std::string{e.what()});
    }
}

void Ws2801RemoteServer::requestBufferWorker()
{
    while (this->run) {
        std::function<void(void)> exec;
        {
            std::lock_guard<std::mutex> lk{this->requestBufferMutex};
            if (this->requestBuffer.size() > 0) {
                exec = this->requestBuffer.front();
                this->requestBuffer.pop();
            } else {
                ::sched_yield();
                continue;
            }
        }
        exec();
        ::sched_yield();
    }
}
