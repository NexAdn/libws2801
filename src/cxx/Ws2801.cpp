#include "ws2801/Ws2801.hpp"

#include "ws2801/ws2801.h"

Ws2801::Ws2801(size_t numPixels)
{
    this->ws = ws2801_create(numPixels);
    if (ws2801_init(this->ws) < 0)
        this->initialized = false;
    else
        this->initialized = true;
}

Ws2801::~Ws2801()
{
    ws2801_deinit(this->ws);
    ws2801_destroy(this->ws);
}

Ws2801::operator bool() const
{
    return this->initialized;
}

void Ws2801::write(size_t pinNo, uint32_t color, bool instant)
{
    if (!this->initialized || pinNo >= this->ws->num_pixels)
        return;

    ws2801_write(this->ws, pinNo, color);
    if (instant)
        this->show();
}

void Ws2801::write(size_t pinNo, uint8_t r, uint8_t g, uint8_t b, bool instant)
{
    this->write(pinNo, Ws2801::ctoi(r, g, b), instant);
}

uint32_t Ws2801::read(size_t pixNo) const
{
    return Ws2801::ctoi(this->ws->pixels[pixNo * 3], this->ws->pixels[pixNo * 3 + 1],
                        this->ws->pixels[pixNo * 3 + 2]);
}

void Ws2801::show() const
{
    ws2801_show(this->ws);
}

void Ws2801::clear(bool instant)
{
    for (size_t i = 0; i < this->ws->num_pixels; i++) {
        this->write(i, 0x0, false);
    }
    if (instant)
        this->show();
}

uint32_t Ws2801::ctoi(const uint8_t r, const uint8_t g, const uint8_t b)
{
    return ws2801_ctoi(r, g, b);
}

std::tuple<uint8_t, uint8_t, uint8_t> Ws2801::itoc(const uint32_t c)
{
    return {(c & 0xff0000) >> 16, (c & 0x00ff00) >> 8, c & 0x0000ff};
}
