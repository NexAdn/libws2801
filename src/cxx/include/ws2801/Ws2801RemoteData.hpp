/**
 * @file Ws2801RemoteData.hpp
 * @brief Definitions and classes used for network data transfer and serialization
 */
#ifndef WS2801REMOTEDATA_HPP__
#define WS2801REMOTEDATA_HPP__
#pragma once

#include <cstddef>
#include <cstdint>
#include <list>
#include <string>
#include <vector>

/**
 * Remote data types and their respective binary values. Can be combined using bitwise or.
 */
enum class Ws2801RemoteDataType : std::int64_t
{
    INVALID = 0x01, //< Response to an invalid request
    COMMAND = 0x02, //< Command request
    DATA = 0x04,    //< Pixel data request
    SYNC = 0x08,    //< Synchronous call (waiting for a response)
    RESPONSE = 0x10 //< Response to a request
};

/**
 * Remote command types and their respective binary values. Only one command per transmission is
 * recognized.
 */
enum class Ws2801RemoteDataCommandType : std::uint8_t
{
    SHOW = 0x01,
    CLEAR = 0x02
};

Ws2801RemoteDataType operator|(const Ws2801RemoteDataType lhs, const Ws2801RemoteDataType rhs);
/**
 * Calculate (lhs & rhs) != 0;
 */
bool operator==(const Ws2801RemoteDataType lhs, const Ws2801RemoteDataType rhs);

/**
 * Data serialization class
 */
class Ws2801RemoteData
{
public:
    /**
     * Create an empty data serializer
     *
     * @param rdt All remote data types combined using bitwise or.
     * @param maxSize The maximum size of the transmitted data. Everything larger than the maximum
     * size is cut off silently!
     */
    Ws2801RemoteData(Ws2801RemoteDataType rdt, std::size_t maxSize);
    /**
     * Create a data serializer and fill it with initial data
     *
     * @param data     Pointer to the first element of the data array
     * @param dataSize Size of the data in bytes
     */
    Ws2801RemoteData(const void* data, std::size_t dataSize);
    /**
     * Create a data serializer and fill it with initial data
     *
     * @param data Vector containing data
     */
    Ws2801RemoteData(std::vector<std::uint8_t> data);
    Ws2801RemoteData(const Ws2801RemoteData& other);
    ~Ws2801RemoteData() = default;

    // std::pair<std::uint8_t*, std::size_t> serialize() const;
    std::size_t getCurrentSize() const;
    std::size_t getMaxSize() const;
    // void serialize(void* data, std::size_t dataSize) const;
    /**
     * Serialize the data to a vector. Can be used for data transmission without any further
     * modification.
     */
    std::vector<std::uint8_t> serialize() const;

    Ws2801RemoteData& addBool(bool val) noexcept;
    Ws2801RemoteData& addInt8(std::int8_t val) noexcept;
    Ws2801RemoteData& addUInt8(std::uint8_t val) noexcept;
    Ws2801RemoteData& addInt16(std::int16_t val) noexcept;
    Ws2801RemoteData& addUInt16(std::uint16_t val) noexcept;
    Ws2801RemoteData& addInt32(std::int32_t val) noexcept;
    Ws2801RemoteData& addUInt32(std::uint32_t val) noexcept;
    Ws2801RemoteData& addInt64(std::int64_t val) noexcept;
    Ws2801RemoteData& addUInt64(std::uint64_t val) noexcept;

    /**
     * Extract a value out of the front of the data array.
     *
     * Extracted data is deleted afterwards!
     */
    template <typename T>
    T get();

    /**
     * Maximum size of a command
     */
    static constexpr std::size_t c2sCommandSize{2 * sizeof(std::int64_t)
                                                + 3 * sizeof(std::uint8_t)};
    /**
     * Maximum size of an RGB data packet
     */
    static constexpr std::size_t c2sDataSize{c2sCommandSize};

private:
    std::list<int8_t> values;
    const std::size_t maxSize;
};

#endif // WS2801REMOTEDATA_HPP__
