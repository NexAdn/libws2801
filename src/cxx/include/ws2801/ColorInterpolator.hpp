/**
 * @file ColorInterpolator.hpp
 *
 * Color interpolation utility
 */

#include <cstdint>
#include <tuple>

/**
 * Utility for color interpolation.
 *
 * This class really just combines storing an integer and the ability to generate a color value from
 * this integer as well was a method to modify the integer.
 */
class ColorInterpolator
{
public:
    /**
     * @param pos Optional initial position.
     */
    ColorInterpolator(const uint8_t pos = 0);
    ~ColorInterpolator();

    /**
     * @return The calculated color as 24 bit numeric value.
     */
    operator uint32_t() const;
    /**
     * @return The calculated color as three single grayscale value bytes
     */
    operator std::tuple<uint8_t, uint8_t, uint8_t>() const;

    /**
     * Returns a rainbow color for a pixel of the rainbow.
     *
     * Creates a rainbow effect across the given amount of pixels and returns the color value of the
     * pixel at the given offset. Can be interpolated using {@link #inc} and {@link #dec}.
     * @param  numPixels The amount of pixels to create the rainbow effect on.
     * @param  pixelNo   The pixel offset from the first pixel whose color value to get.
     * @return           The calculated color value of the pixel at the given offset.
     */
    uint32_t rainbow(const size_t numPixels, const size_t pixelNo) const;

    /**
     * Increase the interpolation position by the given value
     */
    ColorInterpolator& inc(const uint8_t pos);
    /**
     * Decrease the interpolation position by the given value
     */
    ColorInterpolator& dec(const uint8_t pos);

    /**
     * Calculate a 24 bit numeric color value from the given position
     *
     * This function utilizes a 256-step interpolation to get a color.
     */
    static uint32_t getColor(const uint8_t pos);

private:
    uint8_t pos;
};
