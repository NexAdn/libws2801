#ifndef WS2801_HPP__
#define WS2801_HPP__
#pragma once

#include <memory>
#include <tuple>

#include "Ws2801Base.hpp"

extern "C" typedef struct ws2801 ws2801_t;

/**
 * Class representing a ws2801 SPI device.
 *
 * Important: Use {@link Ws2801Base} to pass access to a ws2801 in functions / classes.
 *
 * This class is a C++ wrapper to the C implementation of this ws2801 library.
 * Methods are named similar, but access to device information and internal storages has been
 * restricted, so that the user is forced to use the methods provided to write to the SPI device.
 *
 * Additionally, this class adds some helper methods which are not present in the bare C interface.
 *
 * Copy construction and copy assigment have been deleted.
 */
class Ws2801 : public Ws2801Base
{
public:
    /**
     * Create and initialize a new ws2801.
     *
     * @param numPixels Amount of pixels connected (1m strip should equal 32 bits)
     */
    Ws2801(size_t numPixels = 32);
    ~Ws2801();

    Ws2801(const Ws2801&) = delete;
    Ws2801& operator=(const Ws2801&) = delete;

    /**
     * Check initialization status.
     *
     * Casting to bool returns whether or not the ws2801 instance has been initialized correctly.
     */
    operator bool() const override;

    /**
     * Write to the internal pixel store and optionally send to SPI.
     *
     * This method primarily writes color information to its internal pixel storage.
     *
     * @param pixelNo The pixel to write color information to
     * @param color   RGB color as 24 bit word (see {@link #ctoi}).
     * @param instant Whether or not to perform {@link #show} after setting the pixel color. Does
     * not execute, if the pixel somehow cannot be set.
     */
    void write(size_t pixelNo, uint32_t color, bool instant = false) override;
    /**
     * Write to the internal pixel store and optionally send to SPI.
     *
     * This method overload takes three 8 bit grayscale values as color information instead of a 24
     * bit RGB color value.
     *
     * @param pixelNo The pixel to write color information to
     * @param r       The 8 bit grayscale red amount.
     * @param g       The 8 bit grayscale green amount.
     * @param b       The 8 bit grayscale blue amount.
     * @param instant Whether or not to perform {@link #show} after setting the pixel color. Does
     * not execute, if the pixel somehow cannot be set.
     */
    void write(size_t pixelNo, uint8_t r, uint8_t g, uint8_t b, bool instant = false) override;

    /**
     * Read a pixel from the internal pixel store
     */
    uint32_t read(size_t pixelNo) const override;

    /**
     * Write the internal pixel storage to SPI, i.e. perform any pixel/color changes introduced by
     * {@link #write} on the actual hardware.
     */
    void show() const override;

    /**
     * Clear the pixels (turn all pixels off)
     * @param instant Whether or not to perform {@link #show} afterwards.
     */
    void clear(bool instant = false) override;

    /**
     * Convert 8 bit grayscale values to 24 bit RGB color values
     * @return   A 24 bit RGB color value. The leftmost byte is left out, RGB color information is
     * written left to right after the first byte.
     */
    static uint32_t ctoi(const uint8_t r, const uint8_t g, const uint8_t b);
    /**
     * Convert 24 bit RGB color values to its 8 bit grayscale values
     * @return A tuple of 8 bit grayscale values representing the given 24 bit color value in the
     * order: red, green, blue.
     */
    static std::tuple<uint8_t, uint8_t, uint8_t> itoc(const uint32_t c);

private:
    ws2801_t* ws;
    bool initialized{false};
};

#endif // WS2801_HPP__
