#ifndef WS2801EXCEPTION_HPP__
#define WS2801EXCEPTION_HPP__
#pragma once

#include <exception>
#include <string>

/**
 * General exception class for libws2801xx
 */
class Ws2801Exception : public std::exception
{
public:
    /**
     * @param errNo A unique numeric value identifying the exact exception
     * @param what  A human readable excaption description
     */
    Ws2801Exception(int errNo, std::string what);
    ~Ws2801Exception() = default;
    const char* what();
    int errNo();

private:
    int errNo_;
    std::string what_;
};

#endif // WS2801EXCEPTION_HPP__
