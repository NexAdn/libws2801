#ifndef WS2801REMOTESERVER_HPP__
#define WS2801REMOTESERVER_HPP__
#pragma once

#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

#include <boost/asio.hpp>

#include "Ws2801.hpp"

/**
 * {@link Ws2801Base} wrapper with TCP server capabilities.
 *
 * This class inherits all functionalities of a wrapped {@link Ws2801Base} instance and extends them
 * by a TCP interface to allow network access to the ws2801. Commands can be sent using {@link
 * Ws2801RemoteClient} which is the counterpart of this class.
 *
 * functionality (and related errors) and enable output to stdout respectively.
 */
class Ws2801RemoteServer : public Ws2801Base
{
public:
    Ws2801RemoteServer() = delete;
    /**
     * @param ws        The {@link Ws2801Base} instance to wrap the server around
     * @param portNo    The port to listen on (default is 19638)
     */
    Ws2801RemoteServer(Ws2801Base& ws, unsigned short portNo = 19638);
    ~Ws2801RemoteServer();

    operator bool() const override;

    /**
     * Start the server
     */
    void start();
    /**
     * @return Whether or not the server is running
     */
    bool isRunning() const;
    /**
     * Stop the server
     *
     * This method blocks until all network threads have been shut down properly
     */
    void stop();

    void write(size_t pixelNo, uint32_t color, bool instant = false) override;

    void write(size_t pixelNo, uint8_t r, uint8_t g, uint8_t b, bool instant = false) override;

    uint32_t read(size_t pixelNo) const override;

    void show() const override;

    void clear(bool instant = false) override;

protected:
    /**
     * Implementation of the network listener and message handler threads
     */
    void networkListener();
    /**
     * Request buffer which executes all calls to the {@link Ws2801} methods in the order in which
     * they came in. Should be used inside {@link #networkListener} and all derivatives.
     */
    void requestBufferWorker();

private:
    Ws2801Base& ws;

    bool run{true};

    std::thread networkListenerThread;
    std::vector<std::thread> networkHandlerThreads;

    std::mutex requestBufferMutex;
    std::queue<std::function<void(void)>> requestBuffer;
    std::thread requestBufferWorkerThread;

    boost::asio::io_context ioContext;
    std::unique_ptr<boost::asio::ip::tcp::acceptor> acceptor;
};

#endif // WS2801REMOTESERVER_HPP__
