#ifndef WS2801_REMOTECLIENT_HPP__
#define WS2801_REMOTECLIENT_HPP__
#pragma once

#include <string>
#include <vector>

#include <boost/asio.hpp>

#include "Ws2801Base.hpp"

/**
 * ws2801 network client
 *
 * This class is used to access a {@link Ws2801RemoteServer} via TCP to enable remote hardware
 * access to a ws2801.
 *
 * The methods are implementations of the {@link Ws2801Base} interface, but using a remote server
 * for hardware access.
 *
 * Commands can be received and executed using {@link Ws2801RemoteServer} which
 * is the counterpart of this class.
 */
class Ws2801RemoteClient : public Ws2801Base
{
public:
    Ws2801RemoteClient() = delete;
    /**
     * @param address The IP address or FQDN of the server to connect to
     * @param portNo  The port number of the server to connect to
     * @param pixelNo The amount of pixels connected on the server side
     */
    Ws2801RemoteClient(std::string address, unsigned short portNo = 19638, size_t pixelNo = 32);
    ~Ws2801RemoteClient();

    Ws2801RemoteClient(const Ws2801RemoteClient&) = delete;
    Ws2801RemoteClient& operator=(const Ws2801RemoteClient&) = delete;

    operator bool() const override;

    void write(size_t pixelNo, uint32_t color, bool instant = false) override;
    void write(size_t pixelNo, uint8_t r, uint8_t g, uint8_t b, bool instant = false) override;

    uint32_t read(size_t pixelNo) const override;

    void show() const override;

    void clear(bool instant = false) override;

private:
    size_t numPixels;
    std::vector<uint32_t> pixels;

    boost::asio::io_context ioContext;
    std::unique_ptr<boost::asio::ip::tcp::socket> socket;
};

#endif // WS2801_REMOTECLIENT_HPP__
