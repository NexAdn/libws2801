#ifndef WS2801BASE_HPP__
#define WS2801BASE_HPP__
#pragma once

#include <cstddef>
#include <cstdint>

/**
 * Interface for all classes representing SPI access to a ws2801.
 *
 * This class must be inherited directly or indirectly by all classes which allow SPI interaction
 * with a ws2801. It is the preferred type to be used when definining methods that require a {@link
 * Ws2801} as parameter as this method enables the end user to use both local hardware and network
 * hardware access (through {@link Ws2801} and {@link Ws2801RemoteClient} respectively).
 */
class Ws2801Base
{
public:
    /**
     * Check initialization status.
     *
     * Casting to bool returns whether or not the ws2801 instance has been initialized correctly.
     */
    virtual operator bool() const = 0;

    /**
     * Write to the internal pixel store and optionally send to SPI.
     *
     * This method primarily writes color information to its internal pixel storage.
     *
     * @param pixelNo The pixel to write color information to
     * @param color   RGB color as 24 bit word (see {@link #ctoi}).
     * @param instant Whether or not to perform {@link #show} after setting the pixel color. Does
     * not execute, if the pixel somehow cannot be set.
     */
    virtual void write(size_t pixelNo, uint32_t color, bool instant = false) = 0;
    /**
     * Write to the internal pixel store and optionally send to SPI.
     *
     * This method overload takes three 8 bit grayscale values as color information instead of a 24
     * bit RGB color value.
     *
     * @param pixelNo The pixel to write color information to
     * @param r       The 8 bit grayscale red amount.
     * @param g       The 8 bit grayscale green amount.
     * @param b       The 8 bit grayscale blue amount.
     * @param instant Whether or not to perform {@link #show} after setting the pixel color. Does
     * not execute, if the pixel somehow cannot be set.
     */
    virtual void write(size_t pixelNo, uint8_t r, uint8_t g, uint8_t b, bool instant = false) = 0;

    /**
     * Read a pixel from the internal pixel store
     */
    virtual uint32_t read(size_t pixelNo) const = 0;

    /**
     * Write the internal pixel storage to SPI, i.e. perform any pixel/color changes introduced by
     * {@link #write} on the actual hardware.
     */
    virtual void show() const = 0;

    /**
     * Clear the pixels (turn all pixels off)
     * @param instant Whether or not to perform {@link #show} afterwards.
     */
    virtual void clear(bool instant = false) = 0;
};

#endif // WS2801BASE_HPP__
