# libws2801 – A C/C++ SPI interface for ws2801 LED strips
This library aims to provide simple C and C++ interfaces to ws2801 LED strips connected via to a Raspberry Pi via SPI (although it should work on other devices providing `/dev/spidev0.0` as well).

## Dependencies
* For now, only hardware SPI via `/dev/spidevX.Y` (X or Y be 0 or 1)
* Linux spidev.h header file
* Boost.Asio

## Building
* Clone the Git repository: `git clone https://gitlab.com/nexadn/libws2801.git`
* Create and cd into build directory: `mkdir -p libws2801/build && cd libws2801/build`
* Run CMake: `cmake ..`
* Compile: `make`
* (Optionally) Install to `/usr/local`: `sudo make install`
* (Optionally) Run some of the example executables: `sudo ./example`, `sudo ./examplexx`, `sudo ./clock1`, ...

## Documentation
* Inside your build directory, run `make doc`
* HTML Documentation is inside `.../docs/html`
* PDF Documentation can be compiled running `make` inside `.../docs/pdf`
* For the general workflow, see the examples (`.../src/example/c` (C) and `.../src/example/cxx` (C++))

## Licensing
Copyright (C) 2018 The libws2801 authors

This project is licensed under the terms of the GNU General Public License Version 3 or later. You can find the license contained in the LICENSE file.
